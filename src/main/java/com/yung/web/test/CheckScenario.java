package com.yung.web.test;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

public interface CheckScenario extends BasicScenario {
    
    public void check(WebDriver driver, Properties prop, App app);
    
}