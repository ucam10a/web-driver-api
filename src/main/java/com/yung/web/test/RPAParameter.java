package com.yung.web.test;

public class RPAParameter {

	private Integer processId = 0;
	
	private Integer driverId = 0;
	
	private Integer chromeId = 0;
	
    private String jarFilePath;
    
    private String runId;

    public String getRunId() {
        return runId;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    public String getJarFilePath() {
        return jarFilePath;
    }

    public void setJarFilePath(String jarFilePath) {
        this.jarFilePath = jarFilePath;
    }

	public Integer getProcessId() {
		return processId;
	}

	public void setProcessId(Integer processId) {
		this.processId = processId;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public Integer getChromeId() {
		return chromeId;
	}

	public void setChromeId(Integer chromeId) {
		this.chromeId = chromeId;
	}
    
}