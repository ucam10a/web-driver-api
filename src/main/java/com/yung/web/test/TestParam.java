package com.yung.web.test;

import java.util.HashMap;

/**
 * iKOS test values
 * 
 * @author Yung Long Li
 *
 */
public abstract class TestParam {

    public static final String TEST_PASS_PARAMETER_KEY = "TEST_PASS_PARAMETER_KEY";
    
    /** test value map */
    private HashMap<String, Object> testParms = new HashMap<String, Object>();
    
    /**
     * language type
     */
    public enum Language {
        ENGLISH, CHINESE;
    }

    /**
     * get test value
     * 
     * @return test value map
     */
    public HashMap<String, Object> getTestParams() {
        return testParms;
    }
    
    /**
     * set Test Parameter for next test scenario
     * 
     * @param param test parameter
     */
    public void setTestParameter(Object param) {
    	testParms.put(TEST_PASS_PARAMETER_KEY, param);
    }
    
    /**
     * get Test Parameter for current test
     * 
     * @return get test parameter
     */
    public Object getTestParameter() {
        return testParms.get(TEST_PASS_PARAMETER_KEY);
    }
    
    public abstract void setLocale(String locale);

}