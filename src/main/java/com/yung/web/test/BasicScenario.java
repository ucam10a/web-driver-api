package com.yung.web.test;

public interface BasicScenario {

    public static final String NEW_LINE = System.getProperty("line.separator");
    public static final String ERROR_PREFIX = "Test with error:";
    
    public void logMessage(App app, String message);
    
}
