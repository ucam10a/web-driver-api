package com.yung.web.test;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

public interface PrepareScenario extends BasicScenario {
    
    public void prepare(WebDriver driver, Properties prop, App app);
    
}