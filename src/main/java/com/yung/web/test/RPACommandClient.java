package com.yung.web.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.yung.tool.AppLogger;

import java.util.Set;
import java.util.TreeSet;

public class RPACommandClient {

	private static final boolean debug = false;
    private static final AppLogger logger = AppLogger.getLogger(RPACommandClient.class);
    
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    
    public static final String CMD_SEPARATOR = "&&";
    
    public static final String WINDOWS_EXE_CMD = "cmd /c ";
    
    private static Boolean WINDOWS_OS;
    
    private static final ConcurrentHashMap<Integer, RPAParameter> pidMap = new ConcurrentHashMap<Integer, RPAParameter>();
    
    static {
        String OS = System.getProperty("os.name");
        WINDOWS_OS = OS.startsWith("Windows");
    }
    
    public static ConcurrentHashMap<Integer, RPAParameter> getPidMap() {
        return pidMap;
    }
    
    public static boolean isWindows() {
        if (WINDOWS_OS != null) {
            return WINDOWS_OS;
        }
        String OS = System.getProperty("os.name");
        WINDOWS_OS = OS.startsWith("Windows");
        return WINDOWS_OS;
    }
    
    public static String exeRPACommand(Object controller, final RPAParameter parameter, File jarFile, String runId, AppConfig config, String... params) throws IOException, URISyntaxException, InterruptedException {
        
        String rpaName = getRPAName(jarFile.getName());
        String baseDir = jarFile.getParent();
        String driverLogs = baseDir + "/";
        String logDir = App.getCurrentLogDir(driverLogs, rpaName, runId);
        FileUtil.delete(logDir);
        StringBuilder sb = new StringBuilder();
        if (params != null) {
            for (String param : params) {
                sb.append(param + " ");
            }
        }
        StringBuilder prop = new StringBuilder();
        if (config != null) {
            prop.append(config.getProp());
        }
        boolean isRemote = false;
        if (config.getRemoteUrl() != null && !"".equals(config.getRemoteUrl().trim())) {
            isRemote = true;
        }
        
        StringBuilder buf = new StringBuilder();
        String command = "";
        if (isWindows()) {
            command = WINDOWS_EXE_CMD;
        }
        buf.append(command);
        String cmd = buf.toString() + " cd " + baseDir;
        if (isWindows()) {
            cmd = cmd + " && start /b java ";
        } else {
            cmd = cmd + " && java ";
        }
        cmd = cmd + prop.toString() + "-cp " + jarFile.getName() + " com.yung.web.test.ServerComplexTest " + runId + " " + config.getComplexScenarioClsName() + " " + config.getTestParamClsName() + " " + sb.toString();
        if (isWindows()) {
            cmd = cmd + " > NUL ";
        } else {
            cmd = cmd + " &>/dev/null & ";
        }
        logger.info("exe cmd: " + cmd);
        if (isWindows()) {
            synchronized(controller) {
                int[] currentIds = findAllPids("java");
                int[] currentDriverIds = findAllPids("chromedriver.exe");
                int[] currentChromeIds = findAllPids("chrome.exe");
                Runtime.getRuntime().exec(cmd);
                int pid = findPid(currentIds, "java");
                parameter.setProcessId(pid);
                if (isRemote == false) {
                    int driverId = findPid(currentDriverIds, "chromedriver.exe");
                    parameter.setDriverId(driverId);
                    int chromeId = findPid(currentChromeIds, "chrome.exe");
                    parameter.setChromeId(chromeId);
                }
            }
        } else {
            synchronized(controller) {
                int[] currentIds = findAllPids("java");
                int[] currentDriverIds = findAllPids("chromedriver");
                String[] cmds = {"/bin/bash", "-c", cmd};
                Runtime.getRuntime().exec(cmds);
                int pid = findPid(currentIds, "java");
                parameter.setProcessId(pid);
                if (isRemote == false) {
                    int driverId = findPid(currentDriverIds, "chromedriver");
                    parameter.setDriverId(driverId);
                }
            }
        }
        logger.info("pid: " + parameter.getProcessId());
        logger.info("driver id: " + parameter.getDriverId());
        logger.info("chrome id: " + parameter.getChromeId());
        parameter.setJarFilePath(jarFile.getAbsolutePath());
        parameter.setRunId(runId);
        pidMap.put(parameter.getProcessId(), parameter);
        String msg = getExecuteMessage(parameter, jarFile, runId, config.getMaxWaitSec());
        
        return msg;
    }

	private static int[] findWindowsAllPids(String containWord) throws IOException {
        List<Integer> list = new ArrayList<Integer>();
        Process proc = Runtime.getRuntime().exec(WINDOWS_EXE_CMD + "tasklist");
        List<String> lines = new ArrayList<String>();
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        // Read the output from the command
        String s = null;
        while ((s = stdInput.readLine()) != null) {
            lines.add(s);
        }
        if (lines.size() == 0) {
            StringBuilder sb = new StringBuilder();
            BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
            // Read any errors from the attempted command
            while ((s = stdError.readLine()) != null) {
                sb.append(s + App.NEW_LINE);
            }
            throw new RuntimeException(sb.toString());
        }
        for (String line : lines) {
            if (line.contains(containWord)) {
            	if (debug) {
            		logger.info("[debug findWindowsAllPids] line: " + line);
            	}
            	String[] arr = line.split(" ");
                for (int i = 0; i < arr.length; i++) {
                    if (arr[i] != null && !"".equals(arr[i]) && isNumeric(arr[i])) {
                        int pid = Integer.valueOf(arr[i]);
                        list.add(pid);
                        break;
                    }
                }
            }
        }
        int[] ret = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            ret[i] = list.get(i);
        }
        return ret;
    }
    
    private static int[] findUnixAllPids(String containWord) throws IOException {
        List<Integer> list = new ArrayList<Integer>();
        Process proc = Runtime.getRuntime().exec(" ps -ef ");
        List<String> lines = new ArrayList<String>();
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        // Read the output from the command
        String s = null;
        while ((s = stdInput.readLine()) != null) {
            lines.add(s);
        }
        if (lines.size() == 0) {
            StringBuilder sb = new StringBuilder();
            BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
            // Read any errors from the attempted command
            while ((s = stdError.readLine()) != null) {
                sb.append(s + App.NEW_LINE);
            }
            throw new RuntimeException(sb.toString());
        }
        for (String line : lines) {
            if (line.contains(containWord) && !line.contains("/bin/bash")) {
            	if (debug) {
            		logger.info("[debug findUnixAllPids] line: " + line);
            	}
                String[] arr = line.split(" ");
                for (int i = 0; i < arr.length; i++) {
                    if (arr[i] != null && !"".equals(arr[i]) && isNumeric(arr[i])) {
                        int pid = Integer.valueOf(arr[i]);
                        list.add(pid);
                        break;
                    }
                }
            }
        }
        int[] ret = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            ret[i] = list.get(i);
        }
        return ret;
    }

    private static int findPid(int[] currentIds, String processName) throws IOException, InterruptedException {
        // retry 5 times
        for (int t = 0; t < 5; t++) {
            int[] allPids = findAllPids(processName);
            Set<Integer> allPidSet = new TreeSet<Integer>();
            for (int i = 0; i < allPids.length; i++) {
                allPidSet.add(allPids[i]);
            }
            Set<Integer> currentPidSet = new TreeSet<Integer>();
            for (int i = 0; i < currentIds.length; i++) {
                currentPidSet.add(currentIds[i]);
            }
            for (Integer pid : allPidSet) {
                if (!currentPidSet.contains(pid)) {
                    return pid;
                }
            }
            Thread.sleep(3000);
        }
        return 0;
    }
    
    public static int[] findAllPids(String containWord) throws IOException {
        int[] result = null;
        if (isWindows()) {
            result = findWindowsAllPids(containWord);
        } else {
            result = findUnixAllPids(containWord);
        }
        checkPidMap(result);
        return result;
    }
    
    public static void cancelAllProcess() {
        try {
            findAllPids("java");
        } catch (Exception e) {
            logger.warn(e.toString(), e);
        }
        for (Entry<Integer, RPAParameter> entry : pidMap.entrySet()) {
            try {
                File jarFile = FileUtil.getFile(entry.getValue().getJarFilePath());
                cancelProcess(entry.getKey(), entry.getValue().getRunId(), jarFile);
            } catch (IOException e) {
                logger.warn(e.toString(), e);
            }
        }
        pidMap.clear();
    }
    
    public static void cancelProcess(int pid, String logNo, File jarFile) throws IOException {
    	if (debug) {
    		logger.info("[debug cancelProcess] pid: " + pid);
    	}
        findAllPids("java");
        if (!RPACommandClient.getPidMap().containsKey(pid)) {
            throw new RuntimeException("process id: " + pid + " not exist!");
        }
        String baseDir = jarFile.getParent();
        String rpaName = RPACommandClient.getRPAName(jarFile.getName());
        destroyProcess(pid);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String logDirPath = baseDir + "/logs/" + sdf.format(new Date()) + "/" + rpaName + "/" + logNo;
        File tempLog = FileUtil.getFile(logDirPath + "/" + App.TEMP_LOG_NAME);
        if (tempLog.exists()) {
            RPACommandClient.renameTempLog(logDirPath);
        } else {
            logger.warn("tempLog: " + logDirPath + "/" + App.TEMP_LOG_NAME + " not exist!");
        }
    }
    
    private static void checkPidMap(int[] list) {
        Set<Integer> idSet = new HashSet<Integer>();
        for (int id : list) {
            idSet.add(id);
        }
        Set<Integer> pidSet = pidMap.keySet();
        for (Integer id : pidSet) {
            if (!idSet.contains(id)) {
                logger.info("process id: " + id + ", jarFilePath: " + pidMap.get(id).getJarFilePath() + ", runId: " + pidMap.get(id).getRunId() + " not exist then delete!");
                File jarFile = FileUtil.getFile(pidMap.get(id).getJarFilePath());
                String baseDir = jarFile.getParent();
                String rpaName = RPACommandClient.getRPAName(jarFile.getName());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                String logDirPath = baseDir + "/logs/" + sdf.format(new Date()) + "/" + rpaName + "/" + pidMap.get(id).getRunId();
                File tempLog = FileUtil.getFile(logDirPath + "/" + App.TEMP_LOG_NAME);
                if (tempLog.exists()) {
                    RPACommandClient.renameTempLog(logDirPath);
                }
                pidMap.remove(id);
            }
        }
    }

    private static String getExecuteMessage(final RPAParameter parameter, File jarFile, String runId, int waitSec) throws InterruptedException, IOException {
        
        String rpaName = getRPAName(jarFile.getName());
        String baseDir = jarFile.getParent();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String logDirPath = baseDir + "/logs/" + sdf.format(new Date()) + "/" + rpaName + "/" + runId;
        File logDir = FileUtil.getFile(logDirPath);
        logger.info("logDir: " + logDir.getAbsolutePath());
        int sec = 0;
        int level = waitSec / 20;
        boolean tempLogExist = false;
        while (getFileWithPrefix(logDir, App.SERVER_APP_NAME) == null) {
            Thread.sleep(1000);
            sec++;
            if (!logDir.exists()) {
                logDir = FileUtil.getFile(logDirPath);
            }
            if (sec > waitSec) {
                Thread.sleep(level * 1000);
                cancelProcess(parameter.getProcessId(), runId, jarFile);
                return "wait " + waitSec + " seconds then quit!";
            }
            if (sec % level == 0) {
                logger.info("wait " + sec + " seconds!");
                if (!tempLogExist) {
                    File tempLog = FileUtil.getFile(logDirPath + "/" + App.TEMP_LOG_NAME);
                    if (!tempLog.exists()) {
                        if (!getFileWithPrefix(logDir, App.SERVER_APP_NAME).exists()) {
                            throw new RuntimeException(tempLog.getAbsolutePath() + " not found! Please check RPA process!");
                        }
                    }
                }
            }
        }
        logger.info(getFileWithPrefix(logDir, App.SERVER_APP_NAME).getAbsolutePath() + " found!");
        File log = getFileWithPrefix(logDir, App.SERVER_APP_NAME);
        StringBuilder bud = new StringBuilder();
        bud.append(log.getAbsolutePath() + LINE_SEPARATOR);
        BufferedReader br = new BufferedReader(new FileReader(log));
        try {
            String line = br.readLine();
            while (line != null) {
                if (line != null && !line.equals("")) bud.append(line + LINE_SEPARATOR);
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        pidMap.remove(parameter.getProcessId());
        if (debug) {
        	logger.info("[debug getExecuteMessage] pidMap remove: " + parameter.getProcessId());
        }
        return bud.toString();
    }
    
    private static void renameTempLog(String logDirPath) {
        File tempLog = FileUtil.getFile(logDirPath + "/" + App.TEMP_LOG_NAME);
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(tempLog.getAbsolutePath(), true));
            bw.write("Cancel Process ...");
            bw.flush();
        } catch (IOException ioe) {
            logger.error(ioe.toString(), ioe);
        } finally { // always close the file
            if (bw != null) try {
                bw.close();
            } catch (IOException ioe2) {
                logger.warn(ioe2.toString(), ioe2);
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH-mm-ss");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, 1);
        File logFile = FileUtil.getFile(logDirPath + "/" + App.SERVER_APP_NAME + "-" + sdf.format(cal.getTime()) + ".txt");
        tempLog.renameTo(logFile);
    }

    private static void destroyProcess(int pid) throws IOException {
        ExecutorService executorService = Executors.newScheduledThreadPool(1);
        executorService.execute(new Runnable() {
            public void run() {
                try {
                	RPAParameter parameter = pidMap.get(pid);
                	if (debug) {
                		logger.info("[debug destroyProcess] processId: " + parameter.getProcessId());
                		logger.info("[debug destroyProcess] driverId: " + parameter.getDriverId());
                		logger.info("[debug destroyProcess] chromeId: " + parameter.getChromeId());
                		logger.info("[debug destroyProcess] jarFilePath: " + parameter.getJarFilePath());
                		logger.info("[debug destroyProcess] runId: " + parameter.getRunId());
                	}
                	if (parameter != null) {
                		if (isWindows()) {
                        	destroyWindowsProcess(parameter.getProcessId());
                        	destroyWindowsProcess(parameter.getDriverId());
                        	destroyWindowsProcess(parameter.getChromeId());
                        } else {
                            destroyUnixProcess(parameter.getProcessId());
                            destroyUnixProcess(parameter.getDriverId());
                            destroyUnixProcess(parameter.getChromeId());
                        }	
                	}
                	pidMap.remove(pid);
                    logger.info("destory process forceibly!");
                } catch (Exception e) {
                    logger.error(e.toString(), e);
                    // TODO send mail
                }
            }
        });
        executorService.shutdown();
    }
    
    private static void destroyUnixProcess(int pid) throws IOException {
        if (pid != 0) {
            //logger.info("kill process: kill -9 " + pid);
            //Runtime.getRuntime().exec("kill -9 " + pid);
        }
    }

    private static void destroyWindowsProcess(int pid) throws IOException {
        if (pid != 0) {
            //logger.info("taskkill /F /PID " + pid);
            //Runtime.getRuntime().exec("taskkill /F /PID " + pid);
        }
    }

    public static File getFileWithPrefix(File logDir, String namePrefix) {
        if (!logDir.exists()) {
            return null;
        }
        File[] files = logDir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }
        for (File file : files) {
            if (file.getName().startsWith(namePrefix)) {
                return file;
            }
        }
        return null;
    }
    
    public static String getRPAName(String jarName) {
        if (jarName == null || "".equals(jarName)) {
            return null;
        }
        int idx = jarName.lastIndexOf(".");
        if (idx < 0) {
            return null;
        }
        return jarName.substring(0, idx);
    }
    
    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
       
}