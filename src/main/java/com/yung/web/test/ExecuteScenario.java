package com.yung.web.test;

import java.util.Properties;

import org.openqa.selenium.WebDriver;

public interface ExecuteScenario extends BasicScenario {
    
    public void run(WebDriver driver, Properties prop, App app);
    
}