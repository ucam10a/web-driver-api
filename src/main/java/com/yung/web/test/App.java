package com.yung.web.test;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import com.yung.tool.AppLogger;

public interface App {

    public static final AppLogger logger = AppLogger.getLogger(App.class);
    
    public static final String NEW_LINE = BasicScenario.NEW_LINE;
    public static final String CONFIG_NAME = "config.properties";
    public static final String TEMP_LOG_NAME = "temp-log.txt";
    public static final String SERVER_APP_NAME = "ServerTestApp";
    
    public static final String SERVERTESTAPP_MAX_WAIT_SECONDS_KEY = "ServerTestApp.maxWaitSec";
    public static final String SERVERTESTAPP_HEADLESS_KEY = "ServerTestApp.headless";
    public static final String SERVERTESTAPP_RPANAME_KEY = "ServerTestApp.rpaName";
    public static final String SERVERTESTAPP_REMOTEURL_KEY = "ServerTestApp.remoteURL";
    public static final String BROWSER_PROXY_KEY = "ServerTestApp.proxy";
    public static final String DRIVER_LOGS_KEY = "driver.logs";
    
    public void logMessage(String message);
    
    public void logException(Exception e);
    
    public void dumpLog(String filename);
    
    public String getArgument();
    
    public void setTestLocale(Locale locale);
    
    public Locale getTestLocale();
    
    public TestParam getTestParam();
    
    public void setTestParam(TestParam param);
    
    public void checkWait();
    
    public List<String> getLogList();
    
    public String getLogDir();
    
    public void checkFinishLog();
    
    public static void setServerTestMaxWaitSec(int seconds) {
        System.setProperty(SERVERTESTAPP_MAX_WAIT_SECONDS_KEY, seconds + "");
    }
    
    public static int getServerTestMaxWaitSec() {
        String waitSec = System.getProperty(SERVERTESTAPP_MAX_WAIT_SECONDS_KEY);
        if (waitSec != null && !"".equals(waitSec) && RPACommandClient.isNumeric(waitSec)) {
            int ret = new BigDecimal(waitSec).intValue();
            if (ret < 20) {
                return 20;
            }
            return ret;
        } else {
            return 60 * 30;
        }
    }
    
    public static void setServerTestAppHeadless(boolean headless) {
        System.setProperty(SERVERTESTAPP_HEADLESS_KEY, headless + "");
    }
    
    public static boolean getServerTestAppHeadless() {
        boolean headless = Boolean.valueOf(System.getProperty(SERVERTESTAPP_HEADLESS_KEY));
        return headless;
    }
    
    public static void setServerTestAppRPAName(String rpaName) {
        if (rpaName == null) rpaName = ""; 
        System.setProperty(SERVERTESTAPP_RPANAME_KEY, rpaName);
    }
    
    public static String getServerTestAppRPAName() {
        String rpaName = System.getProperty(SERVERTESTAPP_RPANAME_KEY);
        if (rpaName == null) rpaName = "";
        return rpaName;
    }
    
    public static void setServerTestAppRemoteURL(String remoteURL) {
        System.setProperty(SERVERTESTAPP_REMOTEURL_KEY, remoteURL);
    }
    
    public static String getServerTestAppRemoteURL() {
        return System.getProperty(SERVERTESTAPP_REMOTEURL_KEY);
    }
    
    public static void setBrowserProxy(String ip, int port) {
        String proxy = "--proxy-server=http://" +  ip + ":" + port;
        System.setProperty(BROWSER_PROXY_KEY, proxy);
    }
    
    public static String getBrowserProxy() {
        return System.getProperty(BROWSER_PROXY_KEY);
    }
    
    public static String getDirverLogs() {
        String driverLogs = System.getProperty(DRIVER_LOGS_KEY);
        if (driverLogs == null) driverLogs = "";
        return driverLogs;
    }
    
    public static String getCurrentLogDir(String logNo) {
        String driverLogs = getDirverLogs();
        String rpaName = getServerTestAppRPAName();
        return getCurrentLogDir(driverLogs, rpaName, logNo);
    }
    
    public static String getRPADir(String driverLogs, String rpaName) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String rapDir = null;
        if (driverLogs != null && !"".equals(driverLogs)) {
            rapDir = driverLogs + "logs/" + sdf.format(new Date()) + "/" + rpaName;
        } else {
            rapDir = "logs/" + sdf.format(new Date()) + "/" + rpaName;
        }
        return rapDir;
    }
    
    public static String getCurrentLogDir(String driverLogs, String rpaName, String logNo) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String logDir = null;
        if (logNo == null) logNo = "";
        if (driverLogs != null && !"".equals(driverLogs)) {
            logDir = driverLogs + "logs/" + sdf.format(new Date()) + "/" + rpaName + "/" + logNo;
        } else {
            logDir = "logs/" + sdf.format(new Date()) + "/" + rpaName + "/" + logNo;
        }
        return logDir;
    }
    
    public static <T extends BasicScenario> void runTask(boolean headless, WebDriver driver, List<T> scenarios, Properties prop, App app) {
        try {
            if (scenarios.size() > 0) {
                for (BasicScenario test : scenarios) {
                    app.checkFinishLog();
                    String skipStr = prop.getProperty(test.getClass().getSimpleName() + ".skip");
                    boolean skip = Boolean.valueOf(skipStr);
                    if (skip == false) {
                        if (test instanceof PrepareScenario) {
                            app.checkFinishLog();
                            PrepareScenario PrepareTest = (PrepareScenario) test;
                            PrepareTest.prepare(driver, prop, app);
                        }
                        if (test instanceof ExecuteScenario) {
                            app.checkFinishLog();
                            ExecuteScenario exeTest = (ExecuteScenario) test;
                            exeTest.run(driver, prop, app);
                        }
                        if (test instanceof CheckScenario) {
                            app.checkFinishLog();
                            CheckScenario checkTest = (CheckScenario) test;
                            checkTest.check(driver, prop, app);
                        }
                    } else {
                        app.checkFinishLog();
                        test.logMessage(app, "skip " + test.getClass().getSimpleName() + NEW_LINE);
                    }
                }
            }
            logger.info("All Scenario finished!");
            boolean notQuit = Boolean.valueOf(System.getProperty("auto.test.app.not.quit"));
            if (notQuit == false) {
                logger.info("Quit driver");
                driver.quit();
            } else {
                logger.info("Skip to quit driver");
                app.logMessage("Keep Driver!" + NEW_LINE);
            }
        } catch (Exception ex) {
            logger.warn(ex.toString(), ex);
            app.logException(ex);
            logger.info("Exception to quit driver");
            driver.quit();
        } finally {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH-mm-ss");
            logger.info("start to dump log ...");
            app.dumpLog(app.getClass().getSimpleName() + "-" + sdf.format(new Date()) + ".txt");
            logger.info("dump log done!");
        }
    }
    
    public static Properties loadConfigProp(File f, App app) {
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(f));
        } catch (Exception ex) {
            logger.warn(ex.toString(), ex);
            app.logMessage("load properties fail!" + NEW_LINE + ex.toString() + NEW_LINE);
            prop = null;
        }
        return prop;
    }
    
    public static void checkPropDriverDirPath(Properties prop, String driverName) {
        String driverPath = System.getProperty("webdriver.chrome.driver");
        if (driverPath == null || "".equals(driverPath)) {
            String driverDirPath = prop.getProperty("driver.path");
            if (driverDirPath != null && !"".equals(driverDirPath)) {
                File driverFile = FileUtil.getFile(driverDirPath + "/" + driverName);
                if (driverFile.exists()) {
                    System.setProperty("webdriver.chrome.driver", driverFile.getAbsolutePath());
                }
            }
        }
    }
    
    public static void setDriveFile(File f, App app, String driverName) {
        if (RPACommandClient.isWindows() && !f.getName().endsWith(".exe") && !f.getName().endsWith(".EXE")) {
            app.logMessage("Error: file is not exe file, please select exe file" + NEW_LINE);
        } else {
            if (driverName != null && driverName.startsWith("chromedriver")) {
                System.setProperty("webdriver.chrome.driver", f.getAbsolutePath());
            } else {
                throw new RuntimeException("unknown driver: " + driverName);
            }
        }
    }
    
}