package com.yung.web.test;

public class AppConfig {

    private int maxWaitSec = 1800;
    
    private boolean headless = false;
    
    private String rpaName = "";
    
    private String remoteUrl = "";
    
    private String proxy = "";
    
    private String complexScenarioClsName;
    
    private String testParamClsName;
    
    public int getMaxWaitSec() {
        return maxWaitSec;
    }

    public void setMaxWaitSec(int maxWaitSec) {
        if (maxWaitSec < 20) {
            this.maxWaitSec = 20;
        } else {
            this.maxWaitSec = maxWaitSec;
        }
    }

    public boolean isHeadless() {
        return headless;
    }

    public void setHeadless(boolean headless) {
        this.headless = headless;
    }

    public String getRpaName() {
        return rpaName;
    }

    public void setRpaName(String rpaName) {
        if (rpaName != null) {
            this.rpaName = rpaName;
        }
    }

    public String getRemoteUrl() {
        return remoteUrl;
    }

    public void setRemoteUrl(String remoteUrl) {
        if (remoteUrl != null) {
            this.remoteUrl = remoteUrl;
        }
    }

    public String getProxy() {
        return proxy;
    }

    public void setProxy(String proxy) {
        if (proxy != null) {
            this.proxy = proxy;
        }
    }

    public Object getProp() {
        StringBuilder sb = new StringBuilder();
        sb.append("-D" + App.SERVERTESTAPP_MAX_WAIT_SECONDS_KEY + "=" + maxWaitSec + " ");
        sb.append("-D" + App.SERVERTESTAPP_HEADLESS_KEY + "=" + headless + " ");
        if (rpaName != null && !"".equals(rpaName)) {
            sb.append("-D" + App.SERVERTESTAPP_RPANAME_KEY + "=" + rpaName + " ");
        }
        if (remoteUrl != null && !"".equals(remoteUrl)) {
            sb.append("-D" + App.SERVERTESTAPP_REMOTEURL_KEY + "=" + remoteUrl + " ");
        }
        if (proxy != null && !"".equals(proxy)) {
            sb.append("-D" + App.BROWSER_PROXY_KEY + "=" + proxy + " ");
        }
        return sb.toString();
    }

    public String getComplexScenarioClsName() {
        return complexScenarioClsName;
    }

    public void setComplexScenarioClsName(String complexScenarioClsName) {
        this.complexScenarioClsName = complexScenarioClsName;
    }

    public String getTestParamClsName() {
        return testParamClsName;
    }

    public void setTestParamClsName(String testParamClsName) {
        this.testParamClsName = testParamClsName;
    }
    
}
